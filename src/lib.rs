use assert_approx_eq::assert_approx_eq;
use prettytable::*;
use serde_json::value::Value;

#[derive(Debug)]
struct Contributor {
    login: String,
    contributions: u64,
}

#[derive(Debug)]
pub struct Project {
    name: String,
    top_contributor: String,
    top_percentage: f32,
}

pub struct GithubClient {
    client: reqwest::blocking::Client,
}

impl GithubClient {
    pub fn new(token: &str) -> GithubClient {
        let client = reqwest::blocking::Client::builder()
            .user_agent("request")
            .default_headers(
                std::iter::once((
                    reqwest::header::AUTHORIZATION,
                    reqwest::header::HeaderValue::from_str(&format!("Bearer {}", token)).unwrap(),
                ))
                .collect(),
            )
            .build()
            .expect("Couldn't build a client");

        GithubClient { client: client }
    }

    fn query_repositories_per_page(
        &self,
        language: &str,
        page: u32,
        projects_per_page: u32,
    ) -> Vec<Value> {
        let query = vec![
            ("q", format!("language:{}", language)),
            ("sort", "start".to_string()),
            ("order", "desc".to_string()),
            ("page", format!("{}", page)),
            ("per_page", format!("{}", projects_per_page)),
        ];

        let res = self
            .client
            .get("https://api.github.com/search/repositories")
            .query(&query)
            .send();

        match res {
            Ok(r) => {
                let json: Value = serde_json::from_str(&r.text().unwrap()).unwrap();
                json["items"].as_array().unwrap().clone()
            }
            Err(e) => {
                panic!("Couldn't query github for repositories: {}", e);
            }
        }
    }

    fn query_repositories(&self, language: &str, project_count: u32) -> Vec<Value> {
        let mut project_count = project_count;
        let max_per_page = 100;
        let mut page = 1;
        let mut json_repos = Vec::<Value>::new();

        while project_count > 0 {
            let projects_per_page = calculate_projects_per_page(project_count, max_per_page);
            let json_values = self.query_repositories_per_page(language, page, projects_per_page);

            json_repos.extend(json_values);

            project_count = project_count - projects_per_page;
            page = page + 1;
        }

        json_repos
    }

    fn query_projects_contributors(
        &self,
        language: &str,
        project_count: u32,
    ) -> Vec<(String, Vec<Contributor>)> {
        let json_repos = self.query_repositories(language, project_count);

        json_repos
            .iter()
            .filter_map(|i| {
                let res = self
                    .client
                    .get(i["contributors_url"].as_str().unwrap().to_string())
                    .send();

                match res {
                    Ok(r) => {
                        let json: Value = serde_json::from_str(&r.text().unwrap()).unwrap();
                        let contributors = json
                            .as_array()
                            .unwrap()
                            .into_iter()
                            .map(|j| Contributor {
                                login: j["login"].as_str().unwrap().to_string(),
                                contributions: j["contributions"].as_u64().unwrap(),
                            })
                            .collect();
                        Some((i["name"].as_str().unwrap().to_string(), contributors))
                    }
                    Err(_) => None,
                }
            })
            .collect::<Vec<_>>()
    }
}

fn calculate_top_contributor_ratio(contributors: &Vec<Contributor>, top_count: usize) -> f32 {
    let count = if contributors.len() < top_count {
        contributors.len()
    } else {
        top_count
    };

    let top_contributions = contributors
        .iter()
        .take(count)
        .fold(0, |acc, x| acc + x.contributions);
    let top_contributor_ratio = contributors[0].contributions as f32 / top_contributions as f32;

    top_contributor_ratio
}

fn calculate_projects_per_page(projects_left: u32, max_per_page: u32) -> u32 {
    if projects_left < max_per_page {
        projects_left
    } else {
        max_per_page
    }
}

pub fn query_bus_factor_projects(
    client: &GithubClient,
    language: &str,
    project_count: u32,
) -> Vec<Project> {
    let mut projects = client.query_projects_contributors(language, project_count);

    let top_count = 25;
    let bus_factor_1_ratio = 0.75;

    projects
        .iter_mut()
        .filter_map(|p| {
            let (name, contributors) = p;
            contributors.sort_by(|a, b| b.contributions.cmp(&a.contributions));

            let top_contributor_ratio = calculate_top_contributor_ratio(&contributors, top_count);
            if top_contributor_ratio >= bus_factor_1_ratio {
                Some(Project {
                    name: name.to_string(),
                    top_contributor: contributors[0].login.clone(),
                    top_percentage: top_contributor_ratio,
                })
            } else {
                None
            }
        })
        .collect::<Vec<_>>()
}

pub fn prettyprint(projects: &Vec<Project>) {
    let mut table_format = format::TableFormat::new();
    table_format.column_separator(' ');
    table_format.borders(' ');

    let mut table = Table::new();
    table.set_format(table_format);

    for p in projects {
        table.add_row(Row::new(vec![
            Cell::new(format!("project: {}", p.name).as_str()),
            Cell::new(format!("user: {}", p.top_contributor).as_str()),
            Cell::new(format!("percentage: {}", p.top_percentage).as_str()),
        ]));
    }

    table.printstd();
}

#[test]
fn test_calulcate_top_contributor_ratio_all() {
    let contributors = vec![
        Contributor {
            login: "a".to_string(),
            contributions: 8,
        },
        Contributor {
            login: "b".to_string(),
            contributions: 2,
        },
    ];

    let p = calculate_top_contributor_ratio(&contributors, contributors.len());
    assert_approx_eq!(p, 0.8);
}

#[test]
fn test_calculate_top_contributor_ratio_partial() {
    let contributors = vec![
        Contributor {
            login: "a".to_string(),
            contributions: 7,
        },
        Contributor {
            login: "b".to_string(),
            contributions: 2,
        },
        Contributor {
            login: "c".to_string(),
            contributions: 1,
        },
    ];

    let p = calculate_top_contributor_ratio(&contributors, 1);
    assert_approx_eq!(p, 1.);
    let p = calculate_top_contributor_ratio(&contributors, 2);
    assert_approx_eq!(p, 7. / 9.);
    let p = calculate_top_contributor_ratio(&contributors, 3);
    assert_approx_eq!(p, 0.7);
}
