pub use bus_factor::{prettyprint, query_bus_factor_projects, GithubClient};
use clap::Parser;

#[derive(Parser, Debug)]
struct Args {
    #[clap(long)]
    token: String,
    #[clap(long)]
    language: String,
    #[clap(long)]
    project_count: u32,
}

fn main() {
    let args = Args::parse();

    let token = args.token;
    let project_count = args.project_count;
    let language = args.language;

    let github_client = GithubClient::new(token.as_str());
    let projects = query_bus_factor_projects(&github_client, language.as_str(), project_count);
    prettyprint(&projects);
}
